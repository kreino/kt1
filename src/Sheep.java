import java.util.Arrays;

public class Sheep {

   enum Animal {sheep, goat}


   public static void main(String[] param) {
      Animal[] array = new Animal[]{Animal.goat, Animal.sheep, Animal.sheep, Animal.goat, Animal.sheep, Animal.sheep,
              Animal.goat, Animal.goat};
      reorder(array);

      System.out.println(Arrays.toString(array));
   }

   public static void reorder(Animal[] animals) {

      if (animals.length < 1)
         return;
      int j = animals.length - 1;

      for (int i = 0; i < animals.length; i++) {
         for (; j >= i && j > 0; j--) {
            if(animals[i] == Animal.goat){
               break;
            }
            if (animals[j] == Animal.goat ) {
               Animal a = animals[i]; //swap places so goats are in beggining, all three lines in if method
               animals[i] = animals[j];
               animals[j] = a;
               break;
            }

         }
      }

   }
}

